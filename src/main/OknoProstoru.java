/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import adventura.AdventuraZakladni;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import logika.HerniPlan;
import util.ObserverZmenyProstoru;

/**
 *
 * @author Adam
 */
public class OknoProstoru implements ObserverZmenyProstoru {
    private HerniPlan plan;
    private AnchorPane horniAnchorPane ;
    private  Circle teckaCircle ;
    
    
    public OknoProstoru(HerniPlan plan) {
        this.plan = plan;
        plan.zaregistrujPozorovatele(this);
        nastavHorniPanel();
    }
     /**
     * Metoda zaregistruje pozorovatele k hernímu plánu při spuštění nové hry.
     * 
     * @param plan
     */
    public void nastaveniHernihoPlanu (HerniPlan plan){
        this.plan = plan;
        plan.zaregistrujPozorovatele(this);
        this.aktualizuj();
    }

public AnchorPane getPanel() {
           return horniAnchorPane;
 }

private void nastavHorniPanel ()   {
        horniAnchorPane = new AnchorPane();
        ImageView planekImageView = new ImageView(new Image(AdventuraZakladni.class.getResourceAsStream("/zdroje/planek.png"), 400, 250, false, false));
    
         teckaCircle = new Circle(10, Paint.valueOf("red"));
         horniAnchorPane.getChildren().addAll(planekImageView, teckaCircle);
      }
    
public void aktualizuj() {
         AnchorPane.setTopAnchor(teckaCircle, plan.getAktualniProstor().getPosTop());
         AnchorPane.setLeftAnchor(teckaCircle, plan.getAktualniProstor().getPosLeft());

      }

}
