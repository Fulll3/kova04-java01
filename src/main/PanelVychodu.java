/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import logika.HerniPlan;
import logika.Prostor;
import util.ObserverZmenyProstoru;

/**
 *
 * @author Adam
 */
public class PanelVychodu implements ObserverZmenyProstoru {
    private HerniPlan plan;
    ListView<String> list;
    ObservableList<String> data;
    private TextArea centerText;
    private TextField txtField;

    public PanelVychodu(HerniPlan plan,TextArea centralText, TextField txtField) {
        centerText = centralText;
        this.txtField = txtField;
        this.plan = plan;
       plan.zaregistrujPozorovatele(this);
        init();
    }
    private void init() {
        list = new ListView<>();
        data = FXCollections.observableArrayList();
        list.setItems(data);
        list.setPrefWidth(100);
         list.setOnMouseClicked(new EventHandler<MouseEvent>() 
        {
            @Override
            public void handle(MouseEvent click)
            {
                if(!plan.getHra().konecHry())
                {
                    if(!list.getSelectionModel().getSelectedItem().equals("Východy"))
                    {
                      String vstupniPrikaz = "jdi "+list.getSelectionModel().getSelectedItem();
                      String odpovedHry = plan.getHra().zpracujPrikaz("jdi "+list.getSelectionModel().getSelectedItem());


                      centerText.appendText("\n" + vstupniPrikaz + "\n");
                      centerText.appendText("\n" + odpovedHry + "\n");


                      if (plan.getHra().konecHry()) 
                      {
                      txtField.setEditable(false);
                      centerText.appendText(plan.getHra().vratEpilog());
                      }

                    }
                }
                    //plan.upozorniPozorovatele();
            }
        });
        aktualizuj();
}

    public void aktualizuj() {
        data.clear();
        for (Prostor vychod : plan.getAktualniProstor().getVychody()) {
            data.add(vychod.getNazev() );
        }
    }
    
     public ListView<String> getList() {
        return list;
    }
     
     /**
     * Metoda zaregistruje pozorovatele k hernímu plánu při spuštění nové hry.
     * @param plan
     */
    public void nastaveniHernihoPlanu (HerniPlan plan){
        this.plan = plan;
        plan.zaregistrujPozorovatele(this);
        this.aktualizuj();
    }


}



