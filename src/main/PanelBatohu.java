/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import adventura.AdventuraZakladni;
import java.util.Map;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import logika.Batoh;
import logika.HerniPlan;
import logika.IHra;
import logika.Prostor;
import logika.Vec;
import util.ObserverZmenyBatohu;

/**
 *
 * @author Adam
 */
public class PanelBatohu implements ObserverZmenyBatohu {
    private Batoh batoh;
    ListView<Object> list;
    ObservableList<Object> data;
    private HerniPlan plan;
    private IHra hra;
    private TextArea centerText;

    public PanelBatohu(IHra hra,TextArea centerText) {
        this.centerText = centerText;
        this.batoh = hra.getBatoh();
        this.hra = hra;
        plan = hra.getHerniPlan();
        init();
    }
    private void init() {
        list = new ListView<>();
        data = FXCollections.observableArrayList();
        list.setItems(data);
        list.setPrefWidth(125);
        
    list.setOnMouseClicked(new EventHandler<MouseEvent>() 
        {
                @Override
                public void handle(MouseEvent click)
                {
                     
                    if(!plan.getHra().konecHry())
                    {
                            // Pořadí obrázku na který se kliklo
                            int index = list.getSelectionModel().getSelectedIndex() - 1;
                            Map<String,Vec> seznam;
                            seznam = hra.getBatoh().getSeznamVeci();

                            String nazev = "";
                            int pomocna = 0;
                            // hledání obrázku v seznamu
                            for (String x : seznam.keySet()) 
                            {
                               if(pomocna == index)
                               {
                                   nazev = x;
                               }
                               pomocna++;
                            }

                            if(!nazev.equals(""))
                            {
                            String vstupniPrikaz = "poloz "+nazev;
                            String odpovedHry = plan.getHra().zpracujPrikaz("poloz "+nazev);


                            centerText.appendText("\n" + vstupniPrikaz + "\n");
                            centerText.appendText("\n" + odpovedHry + "\n");

                            plan.upozorniPozorovatele();
                            }
                    }
                }
            
        });
        aktualizuj();
}

    public void aktualizuj() {
       data.clear();
       data.add("Věci v Batohu");
        for (Vec vec : batoh.getVeci()) {
            
            ImageView obrazek = new ImageView(new Image(AdventuraZakladni.class.getResourceAsStream("/zdroje/"+vec.getJmeno()+".jpg"), 100, 100, false, false));
            data.add(obrazek);
            
        }
    }
   
    
     public ListView<Object> getList() {
        return list;
    }
     
     /**
     * Metoda zaregistruje pozorovatele k hernímu plánu při spuštění nové hry.
     * @param plan
     */
    public void nastaveniBatohu(Batoh batoh){
        this.batoh = batoh;
        this.aktualizuj();
    }


}



