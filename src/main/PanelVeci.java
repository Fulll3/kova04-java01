/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import adventura.AdventuraZakladni;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import logika.HerniPlan;
import logika.Prostor;
import logika.Vec;
import util.ObserverZmenyProstoru;

/**
 *
 * @author Adam
 */
public class PanelVeci implements ObserverZmenyProstoru {
    private HerniPlan plan;
    ListView<Object> list;
    ObservableList<Object> data;
    private Map veci;
    private Vec vec;
    private Map<String, Vec> seznamVeci;
    private TextArea centralText;

    public PanelVeci(HerniPlan plan, TextArea centralText) {
        this.plan = plan;
        this.centralText = centralText;
        plan.zaregistrujPozorovatele(this);
      for(Prostor prostor : plan.getSeznamProstoru()){
          prostor.zaregistrujPozorovatele(this);
      }
       
        init();
    }
    private void init() {
        list = new ListView<>();
        data = FXCollections.observableArrayList();
        list.setItems(data);
        list.setPrefWidth(100);
list.setOnMouseClicked(new EventHandler<MouseEvent>() 
        {
                @Override
                public void handle(MouseEvent click)
                {
                    if(!plan.getHra().konecHry())
                    {
                            int index = list.getSelectionModel().getSelectedIndex() - 1;

                            ArrayList<Vec> seznam;
                            seznam = plan.getAktualniProstor().getVeci();

                            String nazev = "";
                            int pomocna = 0;

                            for (Vec pomocnaVec : seznam) 
                            {
                               if(pomocna == index)
                               {
                                   nazev = pomocnaVec.getJmeno();
                               }
                               pomocna++;
                            }

                            if(!nazev.equals(""))
                            {
                            String vstupniPrikaz = "seber "+nazev;
                            String odpovedHry = plan.getHra().zpracujPrikaz("seber "+nazev);


                            centralText.appendText("\n" + vstupniPrikaz + "\n");
                            centralText.appendText("\n" + odpovedHry + "\n");

                            plan.upozorniPozorovatele();
                            }
                    }
                }
            
        });
        aktualizuj();
}

    @Override
    public void aktualizuj() {
        data.clear();
        data.add("Věci v Místnosti");
        seznamVeci = plan.getAktualniProstor().getSeznamVeci();
        
        for (String jmenoVeci : seznamVeci.keySet()){
            //centralText.appendText(jmenoVeci);
          ImageView obrazek = new ImageView(new Image(AdventuraZakladni.class.getResourceAsStream("/zdroje/"+jmenoVeci+".jpg"), 100, 100, false, false));
         data.add(obrazek);
        }
        
        
    }
    
     public ListView<Object> getList() {
        return list;
    }
     
     /**
     * Metoda zaregistruje pozorovatele k hernímu plánu při spuštění nové hry.
     * @param plan
     */
    public void nastaveniHernihoPlanu (HerniPlan plan){
        this.plan = plan;
        plan.zaregistrujPozorovatele(this);
        this.aktualizuj();
    }


}



