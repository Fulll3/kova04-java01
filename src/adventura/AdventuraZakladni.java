/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adventura;


import main.OknoProstoru;
import main.PanelVychodu;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import logika.*;
import uiText.TextoveRozhrani;
import javax.imageio.ImageIO;
import main.PanelBatohu;
import main.PanelVeci;
import util.ObserverZmenyProstoru;

/**
 *
 * @author Adam
 */
public class AdventuraZakladni extends Application {
    private  IHra hra ;
    private BorderPane borderPane ; 
    private TextArea centerTextArea ;
    private FlowPane dolniFlowPane ;
    private Label zadejPrikazLabel ;
    private TextField prikazTextField ;
    private AnchorPane horniAnchorPane ;
    private  Circle teckaCircle ;
    private OknoProstoru oknoProstoru;
    private PanelVychodu panelVychodu ;
    private MenuBar menuBar;
    private PanelVeci panelVeci;
    private PanelBatohu panelBatohu;
    private Batoh batoh;
    private HerniPlan herniPlan;
    private ObserverZmenyProstoru PanelBatohu;
    private Prostor prostor;
    private Stage primaryStage;

        @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
            hra = new Hra();
            herniPlan = hra.getHerniPlan();
            prostor = herniPlan.getAktualniProstor();
            batoh = hra.getBatoh();
      
            
          
            borderPane = new BorderPane();
            BorderPane pravy = new BorderPane();
            
            nastavTextArea();
            borderPane.setCenter(centerTextArea);
            nastavDolniPanel() ;
            borderPane.setBottom(dolniFlowPane);
            
            nastavHorniPanel() ;
            oknoProstoru = new OknoProstoru(hra.getHerniPlan());
            borderPane.setTop(oknoProstoru.getPanel());


             panelVychodu = new PanelVychodu(hra.getHerniPlan(), centerTextArea, prikazTextField);
            panelVeci = new PanelVeci(hra.getHerniPlan(), centerTextArea);
            panelBatohu = new PanelBatohu(hra, centerTextArea);
            
            oknoProstoru.nastaveniHernihoPlanu(hra.getHerniPlan());
            //panelVychodu.nastaveniHernihoPlanu(hra.getHerniPlan());
            //panelVeci.nastaveniHernihoPlanu(hra.getHerniPlan());

            
            pravy.setLeft(panelVeci.getList());
            //pravy.setRight(panelVychodu.getList());
            pravy.setRight(panelBatohu.getList());
            borderPane.setRight(pravy);
            borderPane.setLeft(panelVychodu.getList());
             
            initMenu();
            VBox vBox = new VBox();
            vBox.getChildren().addAll(menuBar,borderPane);

            Scene scene = new Scene(vBox, 800, 600);

            primaryStage.setTitle("Adventura");
            primaryStage.setScene(scene);
            prikazTextField.requestFocus();
            primaryStage.show();
            
            batoh.zaregistrujPozorovatele(panelBatohu);
           prostor.zaregistrujPozorovatele(panelVeci);
        }


    /**
     * @param args the command line argumentsaaa
     */
    public static void main(String[] args) {
        if (args.length == 0) {
            launch(args);
        } else {
            
        }

    }
    
    private void  nastavTextArea() {
        centerTextArea = new TextArea();
        centerTextArea.setText(hra.vratUvitani());
       }
private void nastavDolniPanel () {
        dolniFlowPane = new FlowPane();
        zadejPrikazLabel = new Label("Zadej prikaz");
        zadejPrikazLabel.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        prikazTextField = new TextField();
        dolniFlowPane.setAlignment(Pos.CENTER);
        dolniFlowPane.getChildren().addAll (zadejPrikazLabel, prikazTextField );
        
        
        prikazTextField.setOnAction(new EventHandler<ActionEvent>() {

            public void handle(ActionEvent event) {
                String radek = prikazTextField.getText();
                 String text = hra.zpracujPrikaz(radek);
                centerTextArea.appendText("\n" + radek + "\n");
                centerTextArea.appendText("\n" + text + "\n");
                prikazTextField.setText("");
                if (hra.konecHry()) {
                    prikazTextField.setEditable(false);

                }


            }
        });

}
private void nastavHorniPanel () {
        horniAnchorPane = new AnchorPane();

         ImageView planekImageView = new ImageView(new Image(AdventuraZakladni.class.getResourceAsStream("/zdroje/planek.png"), 800, 350, false, false));
        
         teckaCircle = new Circle(10, Paint.valueOf("red"));
         Prostor aktPr = hra.getHerniPlan().getAktualniProstor();
         //AnchorPane.setTopAnchor(teckaCircle, hra.getHerniPlan().getAktualniProstor().getPosTop());
        // AnchorPane.setLeftAnchor(teckaCircle, hra.getHerniPlan().getAktualniProstor().getPosLeft());
         
         
                  AnchorPane.setTopAnchor(teckaCircle, 100.0);
         AnchorPane.setLeftAnchor(teckaCircle, 100.0);
         
         
         horniAnchorPane.getChildren().addAll(planekImageView, teckaCircle);
         


}

private void initMenu() {
        menuBar = new MenuBar();
        
       // --- Menu Soubor
        Menu menuSoubor = new Menu("Soubor");
       
       //  MenuItem novaHra = new MenuItem("Nová hra");
        MenuItem novaHra = new MenuItem("Nová hra",
                new ImageView(new Image(AdventuraZakladni.class.getResourceAsStream("/zdroje/new.gif"))));
       
        novaHra.setAccelerator(KeyCombination.keyCombination("Ctrl+N"));
        MenuItem konec = new MenuItem("Konec");
         konec.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                System.exit(0);
            }
        });

        menuSoubor.getItems().addAll(novaHra, new SeparatorMenuItem(), konec);
      
       novaHra.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                   start(primaryStage);

            }
        });
       
       
       Menu menuNapoveda = new Menu("Nápověda");
        MenuItem oProgramu = new MenuItem("O programu");
         
        oProgramu.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                // Vyskakovací okno
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Adventura Java");
                alert.setHeaderText("Adventura");
                alert.setContentText("Verze 1.0.0");

                alert.showAndWait();
            }
        });

         MenuItem napovedaKAplikaci = new MenuItem("Nápověda k aplikaci");
         napovedaKAplikaci.setAccelerator(KeyCombination.keyCombination("F1"));


         menuNapoveda.getItems().addAll(oProgramu, new SeparatorMenuItem(), napovedaKAplikaci);

        menuBar.getMenus().addAll(menuSoubor, menuNapoveda);

       
       napovedaKAplikaci.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                // obsluha události Nápověda k aplikaci
                // sekundární okno
                Stage stage = new Stage();
                stage.setTitle("Nápověda k aplikaci");
                WebView webview = new WebView();
                webview.getEngine().load(
                        AdventuraZakladni.class.getResource("/zdroje/napoveda.htm").toExternalForm()
                );
                stage.setScene(new Scene(webview, 500, 500));
                stage.show();
            }
        });


    }

   

    
}
