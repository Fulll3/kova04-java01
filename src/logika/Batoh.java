package logika;

 

import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import util.ObserverZmenyBatohu;
/**
 *  Trida Batoh 
 *
 *
 *@author     Alena Buchalcevova
 *@version    z kurzu 4IT101 pro školní rok 2014/2015
 */

public class Batoh
{
public static final int KAPACITA = 3 ;
private Map<String, Vec> seznamVeci ;   // seznam věcí v batohu
private List<ObserverZmenyBatohu> seznamPozorovatelu ;

public Batoh () {
seznamVeci = new HashMap<String, Vec>();
seznamPozorovatelu = new ArrayList<>();
}
 /**
     * Vloží věc do batohu
     *
     *@param  vec  instance věci, která se má vložit
     */
   public void vlozVec (Vec vec) {
     seznamVeci.put(vec.getJmeno(),vec);
     upozorniPozorovatele();
    }
     /**
     * Vrací řetězec názvů věcí, které jsou v batohu

     *@return            řetězec názvů
     */
    public String nazvyVeci () {
        String nazvy = "věci v batohu: ";
        for (String jmenoVeci : seznamVeci.keySet()){
            	nazvy += jmenoVeci + " ";
        }
        return nazvy;
    }
    
    public ArrayList<Vec> getVeci () {
        ArrayList<Vec> veci = new ArrayList<Vec>();
     for (Vec vec : seznamVeci.values()){
            veci.add(vec);
        }

     return veci;
    }
    
      public Map<String, Vec>  getSeznamVeci () {
        return seznamVeci;
    }
        
     /**
     * Hledá věc daného jména a pokud je v batohu, tak ji vrátí a vymaže ze seznamu

     *@param  jmeno   Jméno věci
     *@return            věc nebo
     *                   hodnota null, pokud tam věc daného jména není 
     */
    public Vec vyberVec (String jmeno) {
        Vec nalezenaVec = null;
        if (seznamVeci.containsKey(jmeno)) {
            nalezenaVec = seznamVeci.get(jmeno);
            seznamVeci.remove(jmeno);
            upozorniPozorovatele();
        }   
        return nalezenaVec;
    }
    
    public void zaregistrujPozorovatele(ObserverZmenyBatohu pozorovatel) {
        seznamPozorovatelu.add(pozorovatel);
    }

    public void odregistrujPozorovatele(ObserverZmenyBatohu pozorovatel) {
    seznamPozorovatelu.remove(pozorovatel);
    }

    public void upozorniPozorovatele() {
        for (ObserverZmenyBatohu pozorovatel :seznamPozorovatelu){
        pozorovatel.aktualizuj();
        }
    }
}



