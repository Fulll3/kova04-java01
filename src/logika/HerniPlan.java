package logika;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import util.ObserverZmenyProstoru;


/**
 *  Class HerniPlan - třída představující mapu a stav adventury.
 * 
 *  Tato třída inicializuje prvky ze kterých se hra skládá:
 *  vytváří všechny prostory,
 *  propojuje je vzájemně pomocí východů 
 *  a pamatuje si aktuální prostor, ve kterém se hráč právě nachází.
 *
 *@author     Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova, Alena Buchalcevova
 *@version    z kurzu 4IT101 pro školní rok 2014/2015
 */
public class HerniPlan {
    
    private Prostor aktualniProstor;
    private Prostor viteznyProstor;
    private List<ObserverZmenyProstoru> seznamPozorovatelu ;
    private ArrayList<Prostor> seznamProstoru;
    private IHra hra;
     /**
     *  Konstruktor který vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví halu.
     * @param hra
     */
    public HerniPlan(IHra hra) {
        
        this.hra = hra;
        zalozProstoryHry();
        seznamPozorovatelu = new ArrayList<>();
       

    }
    
    public IHra getHra(){
        return this.hra;
    }
    /**
     *  Vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví domeček.
     */
    private void zalozProstoryHry() {
        
        // vytvářejí se jednotlivé prostory
        Prostor domecek = new Prostor("domecek","domeček, ve kterém bydlí Karkulka", 58.0,37.0);
        Prostor chaloupka = new Prostor("chaloupka", "chaloupka, ve které bydlí babička Karkulky", 200.0,125.0);
        Prostor jeskyne = new Prostor("jeskyne","stará plesnivá jeskyně", 80.0,125.0);
        Prostor les = new Prostor("les","les s jahodami, malinami a pramenem vody", 128.0,35.0);
        Prostor hlubokyLes = new Prostor("hluboky_les","temný les, ve kterém lze potkat vlka", 135.0,80.0);
        
        
         seznamProstoru =  new ArrayList<>();
         
        seznamProstoru.add(domecek);
        seznamProstoru.add(chaloupka);
        seznamProstoru.add(jeskyne);
        seznamProstoru.add(les);
        seznamProstoru.add(hlubokyLes);
        
        // přiřazují se průchody mezi prostory (sousedící prostory)
        domecek.setVychod(les);
        les.setVychod(domecek);
        les.setVychod(hlubokyLes);
        hlubokyLes.setVychod(les);
        hlubokyLes.setVychod(jeskyne);
        hlubokyLes.setVychod(chaloupka);
        jeskyne.setVychod(hlubokyLes);
        chaloupka.setVychod(hlubokyLes);
                
        aktualniProstor = domecek;  // hra začíná v domečku  
        viteznyProstor = chaloupka ;
        les.vlozVec(new Vec("maliny", true));
        les.vlozVec(new Vec("strom", false));  
        hlubokyLes.vlozVec(new Vec("boruvky", true)); 
    }
    
    
    public ArrayList<Prostor> getSeznamProstoru(){
        return this.seznamProstoru;
    }
    /**
     *  Metoda vrací odkaz na aktuální prostor, ve ktetém se hráč právě nachází.
     *
     *@return     aktuální prostor
     */
    
    public Prostor getAktualniProstor() {
        return aktualniProstor;
    }
    
    /**
     *  Metoda nastaví aktuální prostor, používá se nejčastěji při přechodu mezi prostory
     *
     *@param  prostor nový aktuální prostor
     */
    public void setAktualniProstor(Prostor prostor) {
       aktualniProstor = prostor;
       upozorniPozorovatele();

    }
    /**
     *  Metoda vrací odkaz na vítězný prostor.
     *
     *@return     vítězný prostor
     */
    
    
    public Prostor getViteznyProstor() {
        return viteznyProstor;
    }

    public void zaregistrujPozorovatele(ObserverZmenyProstoru pozorovatel) {
    seznamPozorovatelu.add(pozorovatel);
    }

    public void odregistrujPozorovatele(ObserverZmenyProstoru pozorovatel) {
    seznamPozorovatelu.remove(pozorovatel);
    }

    public void upozorniPozorovatele() {
        for (ObserverZmenyProstoru pozorovatel :seznamPozorovatelu){
        pozorovatel.aktualizuj();
        }
    }
}
